import databases

import ormar
import sqlalchemy

DATABASE_URL = "sqlite:///db.sqlite"

database = databases.Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()


async def db_init():
    from .bans import BannedUsers
    engine = sqlalchemy.create_engine(DATABASE_URL)
    metadata.create_all(engine)


class BaseMeta(ormar.ModelMeta):
    metadata = metadata
    database = database