from typing import Optional

import ormar

from scp.database import BaseMeta


class BannedUsers(ormar.Model):
    class Meta(BaseMeta):
        tablename = "gbans"
    user_id: int = ormar.BigInteger(primary_key=True)
    banned: int = ormar.Integer(maximum=1, minimum=0, default=0)  # 1 if the user is banned, else 0
    reason: Optional[str] = ormar.Text(nullable=True, default="")  # Reason for ban


async def insert_ban(user_id: int, reason: str) -> None:
    """
    Inserts a ban into the database
    :param user_id: The id of the user to ban
    :param reason: The reason for the ban
    :return: None
    """
    f = await BannedUsers.objects.get_or_create(user_id=user_id)
    f.banned = 1
    f.reason = reason
    await f.update()


async def remove_ban(user_id: int) -> None:
    """
    Removes a ban from the database
    :param user_id: The id of the user to unban
    :return: None
    """

    u = await BannedUsers.objects.get_or_create(user_id=user_id)
    await u.delete()


async def get_ban(user_id: int) -> str:
    """
    Gets a ban from the database
    :param user_id: The id of the user to get the ban from
    :return: The ban reason or None if the user is not banned
    """
    e = await BannedUsers.objects.get_or_create(user_id=user_id)
    if e.banned == 1:
        return e.reason
    else:
        return None
