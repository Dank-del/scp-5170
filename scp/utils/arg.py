import argparse

class ParseError(Exception):
    def __init__(self, message):
        self.message = message

class ArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ParseError(message)
