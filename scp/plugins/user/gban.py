import argparse
import html
import shlex

from pyrogram import Client
from pyrogram import errors
from pyrogram.types import Message

import scp.utils.arg as arg
from scp import user
from scp.database import bans as sql

gban_arg_parser = arg.ArgumentParser(prog='gban', description='Gban a person.', add_help=False)

gban_arg_parser.add_argument('-u', help='ID of telegram account to be banned.')
gban_arg_parser.add_argument('-r', default='No reason', help='Reason for ban')
gban_arg_parser.add_argument('-noreply', default='false', choices=['false', 'true'],
                             help='toggle to notify with a reply')

ungban_arg_parser = arg.ArgumentParser(prog='ungban', description='Ungban a person', add_help=False)
ungban_arg_parser.add_argument('-u', help='ID of telegram account to be ungbanned.')
ungban_arg_parser.add_argument('-noreply', default='false', choices=['false', 'true'],
                               help='toggle to notify with a reply')


@user.on_message(user.sudo & user.command('gban'))
async def gban(client: Client, message: Message):
    c = message.command

    if len(c) == 1:
        await message.reply(gban_arg_parser.format_usage(), parse_mode=None, disable_web_page_preview=True)
        return
    else:
        text = shlex.split(message.text)[1:]
        try:
            txt = vars(gban_arg_parser.parse_args(text))
        except arg.ParseError as e:
            await message.reply(e, parse_mode='html')
            return
        except argparse.ArgumentError as e:
            await message.reply(html.escape(e), parse_mode='html')
            return
        except SystemExit:
            return
    try:
        u = await user.get_chat(txt.get('u'))
        reason = txt.get('r')
        reply = txt.get('noreply')
        await sql.insert_ban(user_id=u.id, reason=reason)
        log_channel = user._config.getint('scp-5170', 'LogChannel')
        txt = "/gban {} {}".format(user.md.Mention(label=u.id, uid=u.id), reason)
        await client.send_message(chat_id=log_channel, text=txt, parse_mode="md")
        txt = "/fban {} {}".format(user.md.Mention(label=u.id, uid=u.id), reason)
        await client.send_message(chat_id=log_channel, text=txt, parse_mode="md")
        if reply == 'false':
            txt = user.html.Mention(label=html.escape(u.first_name), uid=u.id) + user.html.Bold(
                text=" has been gbanned") + "\n"
            txt += user.html.Bold(text="Reason:  ") + user.html.Italic(text=reason)
            await message.reply(txt, parse_mode="html")
    except (
            errors.exceptions.bad_request_400.PeerIdInvalid,
            errors.exceptions.bad_request_400.BotResponseTimeout,
    ) as err:
        return await message.reply(err, quote=True)


@user.on_message(user.sudo & user.command('ungban'))
async def ungban(client: Client, message: Message):
    c = message.command

    if len(c) == 1:
        await message.reply(ungban_arg_parser.format_usage(), parse_mode=None, disable_web_page_preview=True)
        return
    else:
        text = shlex.split(message.text)[1:]
        try:
            txt = vars(ungban_arg_parser.parse_args(text))
        except arg.ParseError as e:
            await message.reply(e, parse_mode='html')
            return
        except argparse.ArgumentError as e:
            await message.reply(html.escape(e), parse_mode='html')
            return
        except SystemExit:
            return

    try:
        u = await user.get_chat(txt.get('u'))
        reply = txt.get('noreply')
        await sql.remove_ban(user_id=u.id)
        log_channel = user._config.getint('scp-5170', 'LogChannel')
        txt = "/ungban {} ".format(user.md.Mention(label=u.id, uid=u.id))
        await client.send_message(chat_id=log_channel, text=txt, parse_mode="md")
        txt = "/unfban {} ".format(user.md.Mention(label=u.id, uid=u.id))
        await client.send_message(chat_id=log_channel, text=txt, parse_mode="md")
        if reply == 'false':
            txt = user.html.Mention(label=html.escape(u.first_name), uid=u.id) + user.html.Bold(
                text=" has been ungbanned")
            await message.reply(txt, parse_mode="html")
    except (
            errors.exceptions.bad_request_400.PeerIdInvalid,
            errors.exceptions.bad_request_400.BotResponseTimeout,
    ) as err:
        return await message.reply(err, quote=True)
